Portfolio Architectures - A Beginners Guide
=================================================

This is a beginners guide to how portfolio architectures are put together. Follow along and learn step-by-step how to
design, create, and publish your architecture.

See online guide to get started: [A Beginners guide](https://redhatdemocentral.gitlab.io/portfolio-architecture-template)

[![Cover](cover.png)](https://redhatdemocentral.gitlab.io/portfolio-architecture-template)

